---
title: '19A ITT1 Project'
subtitle: 'Project Inductive Load Monitoring'
authors: ['Group 4']
main_author: ''
date: \today
email: '@edu.ucl.dk'
left-header: \today
right-header: Project plan
skip-toc: false
---



# Background

This is the semester project for ITT1 where you will work with different projects, initated by companies. This projectplan will cover the project, and will match topics that are taught in parallel classes.


# Purpose

The main goal is to ....
This is a learning project and the primary objective is for the students to get a good understanding of the challenges involved in making .... and to give them hands-on experience with the implementation.

For simplicity, the goals stated wil evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows

<insert block diagram>

![diagram](images/diagram.PNG)  

<explanation of system diagram>
A SCCT will be used to measure the voltage. This will be done by using a Arduino to read out the measurements.  

Project deliveries are:  

* Report on self evaluation  
* Report on survey  
* Project report  
* Prototype

The project itself will be divided into 3 phases and gitlab will be used as the project management platform.


# Schedule

The project is divided into three phases from now to christmas holidays.

See the lecture plan for details.

Week 37-40 Phase 1  
week 41-45 Phase 2  
week 46-47 Phase 3  
week 48-50 Phase 4


# Organization

The company for who the project is made is called [SEF Energi](https://www.sef.dk/).


# Budget and resources

No monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,


# Risk assessment

a) Not showing up.
Make sure to follow up on why someones not here, or if you're the person who is not showing up,
inform people that you aren't there and why. Additionally if you're not there you can still help with
research and the like.

b) Tech Issues.
Always have backups, backups of backups. Also make sure to have reserve components. Make sure to ask for help
if you can't fix it yourself.
Make sure you know what you're working with, and make sure you've ordered stuff in good time.
Have a plan B.

c) Poor time planning.
Make sure that your work is structured and planned, and make sure that your deadliens aren't too tight.
Identify which parts of the task are more difficult and which tasks are needed for others to be done and plan around that.

d) Teamwork issues.
Find out peoples stenghts and weaknesses to ensure that people are working well together.
Be helpful to your team-mates, if you see someone strugglnig with an issue then help them if you can.
Ask, don't command.


# Stakeholders

[An analysis of the stakeholder is conducted to establish who has an interest in the project. There will be multiple stakeholders with diverse interests in the project.

Possible stakeholders
    • Internal vs. external
    • Positive vs. negative
    • Active vs. passive
A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.
This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.]

TBD: This section is to be completed by the students.
TBD: Insert your relevant external stakeholder.

# Communication

[The stakeholders may have some requirements or requests as to what reports or other output is published or used. Some part of the project may be confidential.
Whenever there is a stakeholder, there is a need for communication. It will depend on the stakeholder and their role in the project, how and how often communication is needed.
In this section, reports, periodic emails, meetings, facebook groups and any other stakeholder communication will be described]

TBD: Students fill out this part based on risk assessment and stakeholders.

The main communication between students will be on discord. external communication will be through email with the emails listed below.

| Names         | email         |
| ------------- |:-------------:|
| Martin        |mart10t7@edu.ucl.dk|
| Jens          |jens365y@edu.ucl.dk|
| Jonas O.      |jona415t@edu.ucl.dk|
| Veysula       |veys0053@edu.ucl.dk|
| Gaia          |gaia0246@edu.ucl.dk|
| Thomas U.     |thom87m3@edu.ucl.dk|
| Victoria      |vict570g@edu.ucl.dk|
| Meena         |meen0037@edu.ucl.dk|
| Marius        |mari30hs@edu.ucl.dk|

# Perspectives

This project will serve as a template for other similar projects in future semesters.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

[Evaluation is about how to gauge is the project was successful. This includes both the process and the end result. Both of which may have consequences on future projects.
The will be some documentation needed at the end (or during) the project, e.g. external funding will require some sort of reporting after the project has finished. This must be described.]

TBD: Students fill out this one also

# References

[Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.]

TBD: Students may add stuff here at their discretion
TBD: or just write "None at this time"
