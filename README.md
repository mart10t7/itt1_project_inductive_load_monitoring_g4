# itt1_project_inductive_load_monitoring_G4


### Group members:

| Names         | email         |
| ------------- |:-------------:|
| Martin        |mart56t6@edu.ucl.dk|
| Jens          |jens365y@edu.ucl.dk|
| Jonas O.      |jona415t@edu.ucl.dk|
| Veysula       |veys0053@edu.ucl.dk|
| Gaia          |gaia0246@edu.ucl.dk|
| Thomas U.     |thom87m3@edu.ucl.dk|
| Victoria      |vict570g@edu.ucl.dk|
| Meena         |meen0037@edu.ucl.dk|
| Marius        |mari30hs@edu.ucl.dk|