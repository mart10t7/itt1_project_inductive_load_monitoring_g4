a) Not showing up. 
Make sure to follow up on why someones not here, or if you're the person who is not showing up, 
inform people that you aren't there and why. Additionally if you're not there you can still help with 
research and the like.  

b) Tech Issues. 
Always have backups, backups of backups. Also make sure to have reserve components. Make sure to ask for help
if you can't fix it yourself. 
Make sure you know what you're working with, and make sure you've ordered stuff in good time. 
Have a plan B. 

c) Poor time planning. 
Make sure that your work is structured and planned, and make sure that your deadliens aren't too tight. 
Identify which parts of the task are more difficult and which tasks are needed for others to be done and plan around that. 

d) Teamwork issues. 
Find out peoples stenghts and weaknesses to ensure that people are working well together. 
Be helpful to your team-mates, if you see someone strugglnig with an issue then help them if you can. 
Ask, don't command. 