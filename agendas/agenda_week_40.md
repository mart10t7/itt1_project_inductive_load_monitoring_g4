# Weekly meeting (week 40)

## Agenda

##### Status on project (ie. show closed tasks in gitlab)
same as 2 weeks ago, only one issue was added, none were completed  
2 newly added issues for research

##### Next steps (ie. show next tasks in gitlab)
research on how to measure current and voltage using induction.

##### Collaboration within the group (ie. any internal issues, fairness of workload, communication)


##### Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)


##### OLA requirements


##### Attendance
7/12  
1 is excused because of other commitments  
We think one has dropped out, haven't seen him in any lecture lately

##### Any other business (AOB)