# Weekly meeting (week 41)

## Agenda

##### Status on project (ie. show closed tasks in gitlab)
milestones were added to GitLab  
research on measering current and voltage with induction  


##### Next steps (ie. show next tasks in gitlab)
Induction expirements  
little more research on induction and ADC  
Making report templates  
Presentation template  


##### Collaboration within the group (ie. any internal issues, fairness of workload, communication)
has improved even more since last week

##### Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)



##### Attendance
Gaia sick  
Frederick dropped out  
Veysula Sick  
Jonas C. and Jan have been missing since day 1?

##### Any other business (AOB)