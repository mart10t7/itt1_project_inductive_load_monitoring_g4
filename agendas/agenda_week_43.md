# Weekly meeting (week 40)

## Agenda

##### Status on project (ie. show closed tasks in gitlab)
no new tasks were closed. induction experiments were completed

##### Next steps (ie. show next tasks in gitlab)


##### Collaboration within the group (ie. any internal issues, fairness of workload, communication)


##### Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)


##### Attendance

Meena absent (she let us know)  
Gaia unknown

##### Any other business (AOB)