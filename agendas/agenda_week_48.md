# Weekly meeting (week 48)

## Agenda 
##### Status on project (ie. show closed tasks in gitlab)
presentation on survey results was done.  
did some testing with split core current transformer  
##### Next steps (ie. show next tasks in gitlab)
writing the OLA  
start on a working prototype  
more tests with scct
##### Collaboration within the group (ie. any internal issues, fairness of workload, communication)

##### Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

##### Attendance
all here
##### Any other business (AOB)