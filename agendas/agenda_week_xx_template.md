# Weekly meeting (week XX)

## Agenda 
##### Status on project (ie. show closed tasks in gitlab)

##### Next steps (ie. show next tasks in gitlab)

##### Collaboration within the group (ie. any internal issues, fairness of workload, communication)
	 
##### Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

##### Attendance

##### Any other business (AOB)